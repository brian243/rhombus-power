# rhombus-power
 Project for Rhombus Power

I began my project with figuring out what the infrastructure would look like based on the request.  I chose to create 
a simple two tier approach to developing both the jenkins master and the docker host in AWS.   I followed the specifications 
and provisioned a linux ami ec2 instance for this purpose. 

After I figured out wha the infrastructure was going to look like, i began to build it as code with terraform.  I ran into some 
hiccups initially and found their documentation on the terraform website to be extremely helpful as I researched my build. 
In terrraform I built a VPC (CIDR 10.0.0.0/16), 4 subnets (/24 CIDR blocks allowing for 250 additional ip addresses), 
2 private and 2 public to enable the use of jump hosts or a DMZ in the future.  I then created the internet gateway and 2 nat 
gateways that are located in the public subnets created. the nat gateways both needed two elastic ip addresses to associate to them.
I created both the public and private routes to include the route table associaions appropriate to the subnets (public or private).
After the routes and subnets were created I created the security group that allows traffic from port 80, 22, 8080, 8000.
I created two instances tagged jenkins and docker repsectively.  the tags will be important when we move to Ansible. 

After provisioning the infrastructure as code, I began to work on creating ansible roles and playbooks for both instances.  
I created an ansible config file that pointed to the inventory yaml, which created a dynamic inventory from AWS.  I then 
created the jenkins instance ansible playbooks.  it consists of two roles. role 1 is the ec2 installation.  in that role, I installed 
all requried services to include jenkins. I set up the playbook to bypass the jenkins admin setup page which allowed me to provision the
plugins with ansible.  After that, I set up the configurations on jenkins.  I set up all required credentials (AWS, GitLab, Docker, GitLab_API) 
and configured the global settings to allow jenkins to access git and other requirements.  

Once the Jenkins server was provisioned I created the file that was going to be hosted on my dockerhub and uploaded into the docker host.
I started with the app first.  I chose a node.js app because it is the easiest to display and change to show my pipeline working.  For that, 
all I did was follow a tutorial from a youtube video from edureka.  They lined out exactly how to create an easy application that can be viewed
on a docker host.  I created the dockerfile using both documentation and videos from "scottyfullstack".  I also used the documentation from 
https://github.com/nodejs/docker-node.git. after the application was built, I created the jenkinsfile.  The jenkinsfile was simple.  I 
consulted the documentation and made sure that I had all the dependencies downloaded to make sure my pipeline worked.  The file steps 
through the build phase, pushing to docker hub and a cleanup phase. next step is to create a kubernetes cluster or ECS and create a deploy 
to docker container stage to make it scaleable. once all that was complete, i pushed my changes to GitLab.  My GitLab was webhooked to jenkins
so the push triggered a build.  The build occured and my image was built and placed on dockerhub.  

My dockerhost needed to be set up next.  I created an ansbile playbook for that instance as well and it downloaded, gave permissions, added user 
to the docker group and started docker in the first ansible role.  in role 2(webserver_install), I set up my ansible playbook to stop all 
possible instances on the machine, delete all non running docker images, pull the new docker image and then run the image.  

I had the listening port set to 8000 for my docker container.  I copied my public ip address and added the port to the end of it (0.0.0.0:8000)
to see my application work.  If i needed to improve this project, I would add in a container orchesttration service (EKS/ECS) to hadle the load balancing
ans provisioning of new instances.  I would mount an EFS so that my files will remain persistent to the cluster and minimal downtime occured. I will also 
use terraform modules to help with the AWS infrastructure build to help clean up my code and make it easier to read and re-build. 

I used alot of references in my build.  I used mainly the documentation, a youtube channel called scottyfull stack, edureka, https://github.com/nodejs/docker-node.git and personal experience from 
past projects.   